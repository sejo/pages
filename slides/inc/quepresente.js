/*
CHAOTIC SOFTWARE : BEWARE

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

Don't expect support, ongoing development, or maintenance. side effects
may include uncomfortable clarity, dry mouth, and/or spiritual revolt.
For your own sanity, PLEASE FORK NOW!

For more information, please refer to <gemini://xj-ix.luxe/chaotic-software/>
*/

let slides,numeros;
let index = 0;
let botondiv;
function setup(){
 // crea botón
 botondiv = document.createElement("DIV");
 botondiv.id = "botondiv";
 let boton = document.createElement("BUTTON");
 boton.innerHTML="inicia presentación";
 boton.title="inicia presentación";
 boton.addEventListener("click",setupSlides);
 botondiv.appendChild(boton);

 // obtiene slides
 slides = document.getElementsByTagName('section');

 // inserta botón antes de la primera slide
 document.body.insertBefore(botondiv, slides[0]); 
}
function setupSlides(){

 // borra botón
 document.body.removeChild(botondiv);
 // escucha teclas
 document.addEventListener("keyup", change );
 // crea contenedor para número de slide
 numeros = document.createElement("DIV");
 numeros.id="numeros";
 document.body.appendChild(numeros);

 // inicializa slides
// slides = document.getElementsByTagName('section');
 let sa = Array.from(slides);
 sa.forEach((e,i)=>{ e.className="slide"; });
 if( location.search != ""){
    let t = location.search.substr(1).split("="); // quita el ?
    if( t[0] == "slide" )
	 index = parseInt( t[1] ) - 1;
 }
 show( index );
}
function show( i ){
  slides[i].style.display="block";
  numeros.innerHTML = (i+1) +" / "+slides.length;
}
function hide( i ){
  slides[i].style.display="none";
}
function change(event){
  if(event.key == 'ArrowRight' && index < slides.length - 1){
    hide( index );
    show( ++index );
  }
  else if(event.key == 'ArrowLeft' && index > 0){
    hide( index );
    show( --index );
  }
}
