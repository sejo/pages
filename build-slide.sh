#!/bin/sh
# ./build.sh src/archivo.md
archivo=$1
#nombre=$(basename $archivo)
nombre=${archivo#src/} #quita prefijo src
newdir="slides/"
newnombre=${nombre%md}html
newpath="${newdir}${newnombre}"

echo "convirtiendo ${archivo} a ${newpath}..."

headerpath="src/header.html"
footerpath="src/footer.html"

# convierte de md a html, reemplaza <hr /> con sections
# agrega header y footer, 
# agrega título
# y guarda archivo

#titulo=$(awk 'NR==1{ gsub(/^# /,""); print $0 }' $archivo)
titulo=$(sed -n "1s/^# //p" $archivo)

pandoc -f markdown -t html $archivo \
	| sed -e "s:<hr />:</section>\n\n<section>:" -e "s/<img /&loading='lazy'/g" \
	| cat $headerpath - $footerpath \
	| awk -v titulo="${titulo}" '/^<title>/{ print "<title>" titulo "</title>"; next } {print $0}' \
	> "${newpath}"

