# pages

slideshows and other files

## build slideshow

the `build-slide.sh` script creates a formatted html file from a given markdown file:

```
# creates slideshow in slides/file.html
./build-slide.sh src/file.md
```

the script uses `pandoc` to work
