# compudanzas

## sesión 3: computación formal

exploraciones grupales de complejidad emergente

guiado por sejo

![](img/iconocompudanzas_64.png)

---

# agenda

* **presentaciones**
* **danza en parejas**: danzasistemas-tag
* **danza grupal**: d-turing
* **recapitulación**

---

# presentaciones

te invito a que nos compartas lo siguiente:

* tu nombre o alias, y pronombres con los cuales referirnos a ti
* ¿qué te trajo al taller de *compudanzas*?
* ¿qué te gustaría recibir del taller?
* ¿qué te gustaría otorgarle al taller?

---

# plan del curso

el taller consiste en 6 sesiones de 2 horas de duración cada una

1. ~~lógica computacional~~
2. ~~autómatas celulares~~
3. **principios de computación formal**
4. máquina universal de turing
5. organización computacional
6. integración

---

# plan del curso

* cada sesión estaremos explorando diferentes conceptos a partir de actividades que propongo 
* les invito a integrarlas y *remixearlas* para que en la última sesión experimentemos / experienciemos actividades propuestas por ustedes

---

# conceptos que abordaremos / bailaremos hoy:

* universo discreto: estados finitos y tiempos discontinuos
* *tag systems*: máquina de post
* máquinas de turing

---

# primera danza: danzasistemas-tag

![](img/iconocompudanzas_64.png)

[{danzasistemas-tag}](https://compudanzas.net/danzasistemas-tag.html)

---

# segunda danza: d-turing

![](img/iconocompudanzas_64.png)

[{d-turing}](https://compudanzas.net/d-turing.html)

---

# recapitulación: conceptos que abordamos / bailamos hoy:

* universo discreto: estados finitos y tiempos discontinuos
* *tag systems*: máquina de post
* máquinas de turing

---

# sigamos en contacto

e-mail: sejo arroba posteo.net

[compudanzas.net](https://compudanzas.net)

![](img/iconocompudanzas_64.png)
