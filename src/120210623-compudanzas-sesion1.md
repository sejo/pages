# compudanzas

introducción a las ciencias computacionales a partir de la coreografía,

o de cómo exploramos y *cuerpeamos* computadoras alternativas.

guiado por sejo

![](img/iconocompudanzas_64.png)

---

# agenda

* **presentaciones**
* **plática**: compudanzas en contexto
* **plática**: plan del curso
* **danza grupal**: ciclo de memoria
* **danza grupal**: danzas compuertas
* **recapitulación**


---

# presentaciones

te invito a que nos compartas lo siguiente:

* tu nombre o alias, y pronombres con los cuales referirnos a ti
* ¿qué te trajo al taller de *compudanzas*?
* ¿qué te gustaría recibir del taller?
* ¿qué te gustaría otorgarle al taller?

---

# compudanzas en contexto

dimensiones:

* pedagógica
* performática
* especulativa

![](img/iconocompudanzas_64.png)

---

# pedagógica

## coloring computers

![](img/montage_coloring-computers.png)

---

# pedagógica

## coloring computers

![](img/montage_dibujos.png)

![](img/montage_hex7segdecoder.png)


---

# performática

## la consagración de la computadora (the rite of computing)

![](img/montage_laconsagracion-riley.png)

---

# performática

## la consagración de la computadora (the rite of computing)

![](img/montage_laconsagracion-01.png)


---

# especulativa

*¿hay algún problema con las computadoras actuales?*

![](img/foto_rare-earth-mine_unknown-fields.jpg)

[Rare Earthenware - Unknown Fields](https://www.youtube.com/watch?v=YMKJ7S7fKOk) |
[Unknown Fields Division - Summer 2014 China Cargo ship expedition](https://web.archive.org/web/20210415134117/http://www.unknownfieldsdivision.com/summer2014china-aworldadriftpart02.html)

---

# especulativa

*¿hay algún problema con las computadoras actuales?*

tal vez...

energía gris (*embodied energy*), explotación, contaminación en producción y descarte (*e-waste*)

cultura de crecimiento ilimitado y de obsolescencia programada, la paradoja de jevons en eficiencia energética, *bloat* en aplicaciones, incremento en consumo energético...

## un pequeño clavado en low-tech magazine

* [How and why I stopped buying new laptops](https://solar.lowtechmagazine.com/2020/12/how-and-why-i-stopped-buying-new-laptops.html)
* [Deslumbrados por la Eficiencia Energética](https://solar.lowtechmagazine.com/es/2018/10/bedazzled-by-energy-efficiency.html)
* [The monster footprint of digital technology](https://solar.lowtechmagazine.com/2009/06/embodied-energy-of-digital-technology.html)

---

# especulativa: *permacomputing*

*¿cómo darle a las computadoras un lugar sustentable y con razón de ser dentro de una civilización humana que tiene un lugar sustentable y con razón de ser dentro de la biósfera?*

![](img/foto_laconsagracion_09.jpg)

traducido de: [viznut: Permacomputing](http://viznut.fi/texts-en/permacomputing.html)

---

# especulativa: términos relacionados

* computing within limits
* benign computing
* collapse informatics
* permacomputing
* small technology
* salvage computing
* low-tech
* solarpunk
* etc...

[Marloes de Valk: A pluriverse of local worlds: a review of Computing within Limits related terminology and practices](https://computingwithinlimits.org/2021/papers/limits21-devalk.pdf)


---

# plan del curso

el taller consiste en 6 sesiones de 2 horas de duración cada una

1. lógica computacional 
2. autómatas celulares
3. principios de computación formal
4. máquina universal de turing
5. organización computacional
6. integración

---

# plan del curso

* cada sesión estaremos explorando diferentes conceptos a partir de actividades que propongo 
* les invito a integrarlas y *remixearlas* para que en la última sesión experimentemos / experienciemos actividades propuestas por ustedes

---

# conceptos que abordaremos / bailaremos hoy:

* universo discreto: estados finitos y tiempos discontinuos
* dígitos binarios (*bits*)
* operaciones lógicas
* aritmética a partir de lógica


---

# primera danza: ciclo de memoria

![](img/iconocompudanzas_64.png)

---

# primera danza: ciclo de memoria

* **alfabeto**: decidimos tres poses: *descanso*, *círcular*, *cuadrada*
* **red**: asignamos a la persona *input* de cada quien, conformando un ciclo
* planteamos la **instrucción** que seguiremos: 
	- observa la pose de tu *input*  
	- cuando suene la palmada y la palabra *cambio*, toma la pose que estabas observando
* **condición inicial**: todes empezamos en la pose de *descanso*
* **escritura**: hay una persona asignada, *escritora*, que en los primeros *cambios* (uno por cada otra persona) elige la pose *círcular* o *cuadrada* a tomar, acabando eso regresa a la pose de descanso, y luego sigue con la instrucción normal
* **experimentos**: ya que la dinámica funcionó, experimentamos con mayores velocidades de *cambios*.

---

# segunda danza: danzas compuertas

![](img/iconocompudanzas_64.png)

---

# segunda danza: danzas compuertas

* **alfabeto**: decidimos dos poses: *circular* y *cuadrada*
* **red**: diagramada a continuación
* **instrucciones** por rol:
	- *input* de la red, decide su pose
	- *compuerta*: con una o dos personas *inputs*:
		- colócate en posición *cuadrada* solo cuando todxs tus *inputs* estén en posición *circular*
		- en cualquier otro caso, colócate en posición *circular*
	- alguna(s) de la(s) *compuerta(s)* son *output* de la red
* **experimentos**: ya que la dinámica funcione, tabulemos la correspondencia entre *inputs* y *outputs* de la red

---

# danzas compuertas: primera red

![](img/and.png)

---

# danzas compuertas: segunda red

![](img/halfadder.png)

---

# recapitulación

## conceptos que abordamos / bailamos:

* universo discreto: estados finitos y tiempos discontinuos
* dígitos binarios (*bits*)
* operaciones lógicas
* aritmética a partir de lógica

---

# sigamos en contacto

e-mail: sejo arroba posteo.net

[compudanzas.net](https://compudanzas.net)

![](img/iconocompudanzas_64.png)
