# hablemos de compudanzas


![](img/foto_laconsagracion-riley-9.jpg)

sejo en pcd quito 2021

---

# agenda

* ¿qué son las compudanzas?
* una pequeña desviación: *permacomputing*
* ¿cómo hemos llegado hasta aquí?
* una invitación

---

# ¿qué son las compudanzas?

lo que queremos es construir y programar computadoras relativamente lentas, grandes e ineficientes, a partir de personas en movimiento siguiendo/jugando instrucciones. 

nos buscamos reapropiar de las maneras computacionales de entender el mundo, para usarlas a nuestro gusto con el fin de proponer otras posibles computadoras, danzas, y/o formas de vivir en comunidad.

![](img/animacion_troc40x.gif)

---

# ¿qué son las compudanzas?

reimaginamos la computación y sus implicaciones.

de una lógica de productividad y eficiencia, y circuitos que destruyen vida, a danzas... bailecitos... computadoras "inútiles" (pero bien bailadas)


![](img/foto_laconsagracion_03.jpg)

---

# ¿qué son las compudanzas? 

el proyecto tiene diferentes salidas:

* taller de compudanzas
* performances
* computadoras para colorear
* otros experimentos

![](img/foto_laconsagracion_07.jpg)

---

# ¿de dónde viene todo esto?

*¿hay algún problema con las computadoras actuales?*

![](img/foto_rare-earth-mine_unknown-fields.jpg)

[Rare Earthenware - Unknown Fields](https://www.youtube.com/watch?v=YMKJ7S7fKOk) |
[Unknown Fields Division - Summer 2014 China Cargo ship expedition](https://web.archive.org/web/20210415134117/http://www.unknownfieldsdivision.com/summer2014china-aworldadriftpart02.html)

---

# ¿de dónde viene todo esto?

*¿hay algún problema con las computadoras actuales?*

tal vez...

energía gris (*embodied energy*), explotación, contaminación en producción y descarte (*e-waste*)

cultura de crecimiento ilimitado y de obsolescencia programada, la paradoja de jevons en eficiencia energética, *bloat* en aplicaciones, incremento en consumo energético...

## un pequeño clavado en low-tech magazine

* [How and why I stopped buying new laptops](https://solar.lowtechmagazine.com/2020/12/how-and-why-i-stopped-buying-new-laptops.html)
* [Deslumbrados por la Eficiencia Energética](https://solar.lowtechmagazine.com/es/2018/10/bedazzled-by-energy-efficiency.html)
* [The monster footprint of digital technology](https://solar.lowtechmagazine.com/2009/06/embodied-energy-of-digital-technology.html)

---

# pequeña desviación: *permacomputing*

*¿cómo darle a las computadoras un lugar sustentable y con razón de ser dentro de una civilización humana que tiene un lugar sustentable y con razón de ser dentro de la biósfera?*

![](img/foto_laconsagracion_09.jpg)

traducido de: [viznut: Permacomputing](http://viznut.fi/texts-en/permacomputing.html)

---

# algunos términos relacionados

* computing within limits
* benign computing
* collapse informatics
* permacomputing
* small technology
* salvage computing
* low-tech
* solarpunk
* etc...

[Marloes de Valk: A pluriverse of local worlds: a review of Computing within Limits related terminology and practices](https://computingwithinlimits.org/2021/papers/limits21-devalk.pdf)

---

# compudanzas: ¿cómo hemos llegado hasta aquí?

![](img/foto_laconsagracion-riley-6.jpg)

---

# compudanzas: ¿cómo hemos llegado hasta aquí?

1. tecnologías digitales para decorar e instigar danza 
2. herramientas digitales para investigación y enseñanza de la danza
3. pensamiento lógico-matemático como método de composición coreográfica
4. complejidad emergente e imaginación de alternativas

---

# 1. tecnologías digitales para decorar e instigar danza

![](img/montage_idis.png)

---

# 2. herramientas digitales para investigación y enseñanza de la danza

![](img/montage_screenshots-tools.png)

---

# 3. pensamiento lógico-matemático como método de composición coreográfica

## DESFASES

![](img/montage_desfases.png)

[DESFASES: Danza, arte electrónico e interactividad](https://ipfs.io/ipfs/QmUVvhLxXjVttut42SCCx9KazUTEsYx9nDfKQRgp91otvU/)

---

# 3. pensamiento lógico-matemático como método de composición coreográfica

## DESFASES

![](img/montage_graficas-desfases.png)

[DESFASES: Progresivo](https://vimeo.com/132879897)

[DESFASES: MCM](https://vimeo.com/174614756)

---

# 4. complejidad emergente e imaginación de alternativas

## the beans computer

![](img/animacion_beanscomputer.gif)

---

# 4. complejidad emergente e imaginación de alternativas

## hamiltonian cards

![](img/animacion_hamiltonian-cards.gif)

---

# 4. complejidad emergente e imaginación de alternativas

## el computador y change-a-bit

![](img/montage_computador-change-a-bit.png)

[video: change-a-bit](https://goblinrefuge.com/mediagoblin/u/sejomagno/m/change-a-bit/)

---

# 4. complejidad emergente e imaginación de alternativas

## coloring computers

![](img/montage_coloring-computers.png)

---

# 4. complejidad emergente e imaginación de alternativas

## coloring computers

![](img/montage_dibujos.png)

![](img/montage_hex7segdecoder.png)

---

# 4. complejidad emergente e imaginación de alternativas

## la consagración de la computadora (the rite of computing)

![](img/montage_laconsagracion-riley.png)

---

# 4. complejidad emergente e imaginación de alternativas

## la consagración de la computadora (the rite of computing)

![](img/montage_laconsagracion-01.png)

---

# el sitio de compudanzas

repositorio de notas, archivos y partituras:

[compudanzas.net](https://compudanzas.net)

![](img/foto_laconsagracion_04.jpg)

---

# la invitación

*¿cómo darle a las computadoras un lugar sustentable y con razón de ser dentro de una civilización humana que tiene un lugar sustentable y con razón de ser dentro de la biósfera?*

![](img/foto_laconsagracion_08.jpg)

traducido de: [viznut: Permacomputing](http://viznut.fi/texts-en/permacomputing.html)

---

# la invitación

*cualquier comunidad que usa una tecnología debería de desarrollar una relación profunda con ella*

*tendría que haber un entendimiendo local de cada aspecto de la tecnología: no solamente respecto al uso práctico de su mantenimiento y producción, sino también sobre sus aspectos culturales, artísticos, ecológicos, filosóficos e históricos.*

*cada comunidad local haría a la tecnología localmente relevante*

traducido de: [viznut: Permacomputing](http://viznut.fi/texts-en/permacomputing.html)

---

# ¿cuál es nuestro rol como artistas computacionales creando en un mundo aparentemente en colapso?

![](img/foto_waste_unknown-fields.png)

---

# ¿cuál es nuestro rol como artistas computacionales creando en un mundo aparentemente en colapso?

las 4 Rs de la agenda de adaptación profunda (*deep adaptation agenda*)

* **resiliencia**: ¿cómo podemos mantener lo que realmente queremos mantener?
* **renuncia**: ¿qué debemos dejar atrás para no empeorar las cosas?
* **restauración**: ¿qué podemos recuperar que nos ayude durante las dificultades y las tragedias futuras?
* **reconciliaciones**: ¿con qué y con quién quiero hacer las paces a medida que nos enfrentamos a la mortalidad mutua?

[Versions of the Deep Adaptation paper - Professor Jem Bendell](https://jembendell.com/2019/05/15/deep-adaptation-versions/)

---

# sigamos en contacto

e-mail: sejo arroba posteo.net

[compudanzas.net](https://compudanzas.net)

![](img/iconocompudanzas_64.png)
