# compudanzas

## sesión 2: autómatas celulares

exploraciones grupales de complejidad emergente

guiado por sejo

![](img/iconocompudanzas_64.png)

---

# agenda

* **presentaciones**
* **repaso** / **calentamiento**
	* **danza grupal**: ciclo de memoria
	* **danza grupal**: danzas compuertas
* **danza grupal**: reglas de wolfram
* **recapitulación**

---

# progresión de las danzas de hoy

* *ciclo de memoria*: 1 entrada, regla sencilla (*buffer*)
* *danzas compuertas*: 1 o 2 entradas, regla lógica (*NOR* o *NOT*)
* *reglas de wolfram*: 3 entradas, reglas de wolfram

---

# presentaciones

te invito a que nos compartas lo siguiente:

* tu nombre o alias, y pronombres con los cuales referirnos a ti
* ¿qué te trajo al taller de *compudanzas*?
* ¿qué te gustaría recibir del taller?
* ¿qué te gustaría otorgarle al taller?

---

# plan del curso

el taller consiste en 6 sesiones de 2 horas de duración cada una

1. ~~lógica computacional~~
2. **autómatas celulares**
3. principios de computación formal
4. máquina universal de turing
5. organización computacional
6. integración

---

# plan del curso

* cada sesión estaremos explorando diferentes conceptos a partir de actividades que propongo 
* les invito a integrarlas y *remixearlas* para que en la última sesión experimentemos / experienciemos actividades propuestas por ustedes

---

# conceptos que abordaremos / bailaremos hoy:

* universo discreto: estados finitos y tiempos discontinuos
* repaso:
	* dígitos binarios (*bits*)
	* operaciones lógicas
	* aritmética a partir de lógica
* autómatas celulares:
	* máquinas de estados
	* complejidad emergente


---

# repaso: primera danza: ciclo de memoria

![](img/iconocompudanzas_64.png)

---

# repaso: primera danza: ciclo de memoria

* **alfabeto**: decidimos tres movimientos: *descanso*, *expansivo*, *implosivo*
* **red**: asignamos a la persona *input* de cada quien, conformando un ciclo
* planteamos la **instrucción** que seguiremos: 
	- observa la pose de tu *input*  
	- cuando suene la palabra *cambio*, toma la pose que estabas observando
* **condición inicial**: todes empezamos en alguno de los tres movimientos

---

# repaso: segunda danza: danzas compuertas

![](img/iconocompudanzas_64.png)

---

# repaso: segunda danza: danzas compuertas

* **alfabeto**: decidimos dos movimientos: *expansivo* e *implosivo*
* **red**: diagramada a continuación
* **instrucciones** por rol:
	- *input* de la red, decide su pose
	- *compuerta*: con una o dos personas *inputs*:
		- realiza el movimiento *expansivo* solo cuando todxs tus *inputs* estén en movimiento *implosivo*
		- en cualquier otro caso, realiza el movimiento *implosivo*
	- alguna(s) de la(s) *compuerta(s)* son *output* de la red

---

# danzas compuertas: primera red

## AND lógico y/o multiplicador de tamaño 1 bit

![](img/and.png)

---

# danzas compuertas: segunda red

![](img/halfadder.png)

---

# tercera danza: autómatas celulares

## reglas de wolfram

![](img/iconocompudanzas_64.png)

---

# autómatas celulares

* **alfabeto**: utilizamos dos poses: *expansiva* e *implosiva*
* **red de relaciones**: formamos un ciclo:
	- cada quien tiene dos vecines, una a su *derecha* y otra a su *izquierda*
* **instrucciones**:
	- cada quien observa el estado propio y el de sus dos vecines, teniendo claro quién está a la *derecha* y quién a la *izquierda*
	- de acuerdo a las tablas de reglas a continuación, cada quien visualiza cuál será su próximo estado
	- en la indicación de *cambio*, todes cambiamos al que identificamos como su próximo estado
* **condición inicial**: determinada por el grupo. a probar:
	- todes en movimiento *implosivo*, excepto una persona
	- todes eligiendo cualquiera de los dos movimientos

---

# autómatas celulares

## regla 90 de wolfram

![regla 90: X es *expansivo* y el punto es *implosivo*](img/rule90.png)

otra forma de leerla: 

* si les dos vecines están en la misma pose, tu siguiente estado es *implosivo*
* si les dos vecines tienen una pose distinta, tu siguiente estado es *expansivo*

(a ese comportamiento lógico también se le llama XOR o EOR, de *exclusive OR*)

---

# autómatas celulares

## regla 30 de wolfram

![regla 30: X es *expansivo* y el punto es *implosivo*](img/rule30.png)

¡ahora sí hay que poner atención al estado propio actual!

---

# autómatas celulares

## regla 110 de wolfram

![regla 110: X es *expansivo* y el punto es *implosivo*](img/rule110.png)

¡ahora sí hay que poner atención al estado actual propio!

---

# autómatas celulares

## comportamientos según wolfram

hablando sobre la *evolución* de este tipo de sistemas:

* Clase I. La evolución lleva a una configuración estable y homogénea, es decir, todas las células terminan por llegar al mismo valor.
* Clase II. La evolución lleva a un conjunto de estructuras simples que son estables o periódicas.
* Clase III. La evolución lleva a un patrón caótico.
* Clase IV. La evolución lleva a estructuras aisladas que muestran un comportamiento complejo (es decir, ni completamente caótico, ni completamente ordenado, sino en la línea entre uno y otro, este suele ser el tipo de comportamiento más interesante que un sistema dinámico puede presentar).

[Autómata celular - Wikipedia](https://es.wikipedia.org/wiki/Aut%C3%B3mata_celular#Era_de_Stephen_Wolfram)

Reglas 90 y 30 son de clase III, regla 110 de clase IV

---

# autómatas celulares

¿cómo se ve nuestro comportamiento en el espacio?

* [rule 90 - Wolfram|Alpha](https://www.wolframalpha.com/input/?i=rule+90)
* [rule 30 - Wolfram|Alpha](https://www.wolframalpha.com/input/?i=rule+30)
* [rule 110 - Wolfram|Alpha](https://www.wolframalpha.com/input/?i=rule+110)

estas reglas las podemos desarrollar en papel también!

---

# autómatas celulares

más:

* [xkcd: A bunch of rocks](https://xkcd.com/505/)

---

# recapitulación

## conceptos que abordamos / bailamos:

* universo discreto: estados finitos y tiempos discontinuos
* repaso:
	* dígitos binarios (*bits*)
	* operaciones lógicas
	* aritmética a partir de lógica
* autómatas celulares:
	* máquinas de estados
	* complejidad emergente

---

# sigamos en contacto

e-mail: sejo arroba posteo.net

[compudanzas.net](https://compudanzas.net)

![](img/iconocompudanzas_64.png)
