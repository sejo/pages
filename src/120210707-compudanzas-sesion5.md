# compudanzas

## sesión 5: arquitectura computacional

guiado por sejo

![](img/iconocompudanzas_64.png)

---

# agenda

* presentaciones?
* modelos de arquitectura computacional
	* papier computer
	* little person computer
* recapitulación de danzas
* plan para la última sesión

---

# presentaciones

te invito a que nos compartas lo siguiente:

* tu nombre o alias, y pronombres con los cuales referirnos a ti
* ¿qué te trajo al taller de *compudanzas*?
* ¿qué te gustaría recibir del taller?
* ¿qué te gustaría otorgarle al taller?

---

# plan del curso

el taller consiste en 6 sesiones de 2 horas de duración cada una

1. ~~lógica computacional~~
2. ~~autómatas celulares~~
3. ~~principios de computación formal~~
4. ~~máquina universal de turing~~
5. arquitectura computacional
6. integración

---

# plan del curso

* cada sesión estaremos explorando diferentes conceptos a partir de actividades que propongo 
* les invito a integrarlas y *remixearlas* para que en la última sesión experimentemos / experienciemos actividades propuestas por ustedes

---

# conceptos que abordaremos hoy:

* universo discreto: estados finitos y tiempos discontinuos
* arquitectura computacional
* código máquina
* lenguaje ensamblador
* ciclo de *fetch* y *execute*

---

# arquitectura computacional: papier computer

* componentes: procesador, memoria, registros
* instrucciones: lenguaje de máquina, y ensamblador
* ciclo de instrucción
* lenguaje ensamblador

---

# arquitectura computacional: little person computer

* [simulador](https://www.peterhigginson.co.uk/lmc/)
* [instrucciones](https://teachcomputerscience.com/lmc-instruction-set/)

---

# conceptos que abordamos hoy:

* universo discreto: estados finitos y tiempos discontinuos
* arquitectura computacional
* código máquina
* lenguaje ensamblador
* ciclo de *fetch* y *execute*

---

# recapitulación de danzas

* ciclo de memoria
* danzas compuertas
* reglas de wolfram
* d-turing
* máquina universal bailable
* danza papier

---

# sesión de integración

te invito a retomar / remixear / revivir alguna de las danzas o actividades que hemos realizado en estas sesiones, dándole tu toque personal.

el objetivo de la última sesión del curso es que como grupo podamos experimentar con lo que prepares y propongas.

---

# sigamos en contacto

e-mail: sejo arroba posteo.net

[compudanzas.net](https://compudanzas.net)

![](img/iconocompudanzas_64.png)
