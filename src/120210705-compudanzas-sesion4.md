# compudanzas

## sesión 4: computación universal

exploraciones grupales de complejidad emergente

guiado por sejo

![](img/iconocompudanzas_64.png)

---

# agenda

* **presentaciones**
* **recapitulación**: d-turing
* **presentación**: otras máquinas de turing
* **danza grupal**: máquina universal bailable
* **discusión**: límites de lo computable

---

# presentaciones

te invito a que nos compartas lo siguiente:

* tu nombre o alias, y pronombres con los cuales referirnos a ti
* ¿qué te trajo al taller de *compudanzas*?
* ¿qué te gustaría recibir del taller?
* ¿qué te gustaría otorgarle al taller?

---

# plan del curso

el taller consiste en 6 sesiones de 2 horas de duración cada una

1. ~~lógica computacional~~
2. ~~autómatas celulares~~
3. ~~principios de computación formal~~
4. **máquina universal de turing**
5. organización computacional
6. integración

---

# plan del curso

* cada sesión estaremos explorando diferentes conceptos a partir de actividades que propongo 
* les invito a integrarlas y *remixearlas* para que en la última sesión experimentemos / experienciemos actividades propuestas por ustedes

---

# conceptos que abordaremos / bailaremos hoy:

* universo discreto: estados finitos y tiempos discontinuos
* máquinas de turing
* máquina universal de turing
* límites de lo computable

---

# recapitulación: d-turing

![](img/iconocompudanzas_64.png)

[{d-turing}](https://compudanzas.net/d-turing.html)

---

# d-turing

## planteamiento

![](img/d-turing_parentesis_VGA.png)

---

# d-turing

## tabla de estados

![](img/tabla_d-turing_parentesis_VGA.png)

---

# otras máquinas

---

# m.u.b.

![símbolos en la cinta](img/mub_simbolos-cinta.png)

---

# m.u.b.

![estado inicial de la cinta](img/mub_cintainicial.png)

---

# m.u.b.

![](img/mub_simbolos.png)

---

# m.u.b.
![los estados en diagrama](img/mub_simbolos-diagrama.png)

---

# recapitulación: conceptos que abordamos / bailamos hoy:

* universo discreto: estados finitos y tiempos discontinuos
* máquinas de turing
* máquina universal de turing
* límites de lo computable

---

# sigamos en contacto

e-mail: sejo arroba posteo.net

[compudanzas.net](https://compudanzas.net)

![](img/iconocompudanzas_64.png)
