#!/bin/sh

for f in srcimg/selec/*.jpg; do convert -verbose $f -scale x500\> slides/img/$(basename $f); done
for f in srcimg/selec/*.png; do convert -verbose $f -scale x500\> slides/img/$(basename $f); done
